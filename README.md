# My project's README

Download PCL:
https://github.com/PointCloudLibrary/pcl

Download Project:
https://bitbucket.org/saad149/shape_analysis/downloads

Follow Steps in:
http://pointclouds.org/documentation/tutorials/compiling_pcl_posix.php

Compile PCL:

1. cd pcl-pcl-1.7.2 && mkdir build && cd build

2. cmake ..

3. make -j2

4. Replace the 4 files in their appropriate directories

5. make -j2 install


replace the following files:

1. pcl/visualization/src/common/shapes.cpp				------> PCL_source/shapes.cpp
2. pcl/visualization/src/pcl_visualizer.cpp				------> PCL_source/pcl_visualizer.cpp
3. pcl/visualization/include/pcl/visualization/pcl_visualizer.h		------> PCL_source/pcl_visualizer.h
4. pcl/visualization/include/pcl/visualization/common/shapes.h		------> PCL_source/shapes.h

