#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/common/common.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/passthrough.h>
#include <pcl/features/normal_3d.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/common/actor_map.h>
#include <pcl/features/principal_curvatures.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/filters/statistical_outlier_removal.h>

#define MINPOINTSCYLINDER 450
#define MINPOINTSPLANE 1200

typedef pcl::PointXYZ PointT;		// For point cloud without RGB values
typedef pcl::PointXYZRGB PointTC;	// For point cloud with RGB values


int main (int argc, char** argv)
{
  //All the objects needed
  pcl::PCDReader pcdreader;
  pcl::PLYReader plyreader;
  pcl::PassThrough<PointT> pass;	// Filter for point cloud without RGB values
  pcl::PassThrough<PointTC> passR;	// Filter for point cloud with RGB values
  pcl::NormalEstimation<PointT, pcl::Normal> ne;
  pcl::SACSegmentationFromNormals<PointT, pcl::Normal> seg_plane;
  pcl::SACSegmentationFromNormals<PointT, pcl::Normal> seg_cylinder;
  pcl::SACSegmentationFromNormals<PointT, pcl::Normal> seg_sphere;
  pcl::PCDWriter writer;
  pcl::ExtractIndices<PointT> extract;
  pcl::ExtractIndices<pcl::Normal> extract_normals;
  pcl::search::KdTree<PointT>::Ptr tree (new pcl::search::KdTree<PointT> ());
  pcl::ModelCoefficients cylinder_coeff;	// Cylinder coefficints used for drawing the cylinder
  pcl::ModelCoefficients cylinder_coeff1;	// Cylinder coefficints used for drawing the cylinder
  pcl::ModelCoefficients plane_coeff;		// Plane coefficints used for drawing the plane
  pcl::ModelCoefficients sphere_coeff;		// Sphere coefficints used for drawing the Sphere
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("Scene"));
  bool plane(true);				//will be later used to see if there was a plane detected or not for 
  bool go_plane(true);

  // Datasets
  pcl::PointCloud<PointT>::Ptr cloud (new pcl::PointCloud<PointT>);
  pcl::PointCloud<PointTC>::Ptr cloudR (new pcl::PointCloud<PointTC>);
  pcl::PointCloud<PointT>::Ptr cloud_filtered (new pcl::PointCloud<PointT>);
  pcl::PointCloud<PointTC>::Ptr cloud_filteredR (new pcl::PointCloud<PointTC>);
  pcl::PointCloud<PointT>::Ptr cloud_plane (new pcl::PointCloud<PointT> ());
  pcl::PointCloud<PointT>::Ptr cloud_sphere (new pcl::PointCloud<PointT> ());
  pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);
  pcl::PointCloud<PointT>::Ptr cloud_filtered2 (new pcl::PointCloud<PointT>);
  pcl::PointCloud<pcl::Normal>::Ptr cloud_normals2 (new pcl::PointCloud<pcl::Normal>);
  pcl::ModelCoefficients::Ptr coefficients_plane (new pcl::ModelCoefficients), coefficients_cylinder (new pcl::ModelCoefficients), coefficients_sphere (new pcl::ModelCoefficients);
  pcl::PointIndices::Ptr inliers_plane (new pcl::PointIndices), inliers_cylinder (new pcl::PointIndices), inliers_sphere (new pcl::PointIndices);

  // Read in the cloud data from file
  std::stringstream input_data;
  input_data << argv[1];
  std::string input = input_data.str();
  if(!input.substr(input.length() - 4, input.length()-1).compare(".pcd")){
	pcdreader.read (input_data.str(), *cloud);	// Reading point cloud without the RGB values
	//  reader.read (input_data.str(), *cloud_filtered2);	// Reading point cloud without the RGB values
	pcdreader.read (input_data.str(), *cloudR);	// Reading point cloud with the RGB values for 
  }
  else{
	plyreader.read (input_data.str(), *cloud);	// Reading point cloud without the RGB values
	//  reader.read (input_data.str(), *cloud_filtered2);	// Reading point cloud without the RGB values
	plyreader.read (input_data.str(), *cloudR);	// Reading point cloud with the RGB values for 	
  }
  std::cout<< input.substr(input.length() - 4, input.length()-1) <<std::endl;
//  reader.read (input_data.str(), *cloud_filteredR);	// Reading point cloud with the RGB values for visualization only
  std::cerr << "PointCloud has: " << cloud->points.size () << " data points." << std::endl;

  // Build a passthrough filter to remove spurious NaNs for Point Cloud WITHOUT RGB values (depth filtering) used for segmentation
  pass.setInputCloud (cloud);
  pass.setFilterFieldName ("z");
  pass.setFilterLimits (0.7, 1.4);
  pass.filter (*cloud_filtered2);
  std::cerr << "PointCloud after filtering has: " << cloud_filtered2->points.size () << " data points." << std::endl;

  // Build a passthrough filter to remove spurious NaNs for Point Cloud WITH RGB values (depth filtering) used for visualization only
  passR.setInputCloud (cloudR);
  passR.setFilterFieldName ("z");
  passR.setFilterLimits (0.7, 1.4);
  passR.filter (*cloud_filteredR);
  std::cerr << "RGB PointCloud after filtering has: " << cloud_filteredR->points.size () << " data points." << std::endl;


  // ----------------------------------------------------
  // -----Open 3D viewer and add the RGB point cloud-----
  // ----------------------------------------------------

  viewer->setBackgroundColor (1, 1, 1);
  pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(cloud_filteredR);
  viewer->addPointCloud<pcl::PointXYZRGB> (cloud_filteredR, rgb, "sample cloud");
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
//  viewer->addCoordinateSystem (1.0);
  viewer->initCameraParameters ();



// Create the filtering object: downsample the dataset using a leaf size of 1cm (reduce to having only 1 point in each 1cm3 volume)
/*  cloud_filtered2.swap(cloud);
  pcl::VoxelGrid<PointT> sor;
  sor.setInputCloud (cloud);
  sor.setLeafSize (0.01f, 0.01f, 0.01f);
  sor.filter (*cloud_filtered2);
  std::cerr << "PointCloud after downsampling has: " << cloud_filtered->points.size () << " data points." << std::endl;
*/

   // Estimate point normals
  ne.setSearchMethod (tree);
  ne.setInputCloud (cloud_filtered2);
  ne.setKSearch (50);
  ne.compute (*cloud_normals);


  //********************************************** Create the segmentation object for Plane segmentation and set all the parameters **********************************************//
  seg_plane.setOptimizeCoefficients (true);
  seg_plane.setModelType (pcl::SACMODEL_NORMAL_PLANE);
  seg_plane.setNormalDistanceWeight (0.1);
  seg_plane.setMethodType (pcl::SAC_RANSAC);
  seg_plane.setMaxIterations (100);
  seg_plane.setDistanceThreshold (0.10);


  //********************************************** Create the segmentation object for Sphere segmentation and set all the parameters **********************************************//
  seg_sphere.setOptimizeCoefficients (true);		// Optional
  seg_sphere.setModelType (pcl::SACMODEL_SPHERE);	// Mandatory
  seg_sphere.setMethodType (pcl::SAC_RANSAC);
  seg_sphere.setMaxIterations (1000);
  seg_sphere.setDistanceThreshold (0.01);
  seg_sphere.setProbability(0.99999);
  seg_sphere.setRadiusLimits (0.05, 0.10);


  //********************************************** Create the segmentation object for Cylinder segmentation and set all the parameters **********************************************//
  seg_cylinder.setOptimizeCoefficients (true);
  seg_cylinder.setModelType (pcl::SACMODEL_CYLINDER);
  seg_cylinder.setMethodType (pcl::SAC_RANSAC);
  seg_cylinder.setNormalDistanceWeight (0.1);
  seg_cylinder.setMaxIterations (10000);
  seg_cylinder.setDistanceThreshold (0.03);
  seg_cylinder.setRadiusLimits (0, 0.1);



  // ****************************************Create the Statistical Outlier Removal object********************************************
  pcl::StatisticalOutlierRemoval<PointT> sor;





  int i = 0, j = 0, k = 0;
  int nr_points = (int) cloud_filtered2->points.size ();
  //  std::cout<<"nr_points: "<<nr_points<<std::endl;
  bool go_cylinder(true);
  bool go_sphere(true);
  while (!viewer->wasStopped ()){
	viewer->spinOnce (100);
	boost::this_thread::sleep (boost::posix_time::microseconds (100000));

  //********************************************** for PLANE segmentation/extraction **********************************************//
//	while (cloud_filtered2->points.size () > 0.2 * nr_points && plane){			// Without downsampling
	while(j<5){

		std::cout<<"\n\n\n*************************************Commencing Plane segmentation:***********************************\n"<<std::endl;
		// Create the segmentation object for the planar model and set all the parameters
		seg_plane.setInputCloud (cloud_filtered2);
		seg_plane.setInputNormals (cloud_normals);
		// Obtain the plane inliers and coefficients
		seg_plane.segment (*inliers_plane, *coefficients_plane);	//Marking the points that belong to the plane
		if (inliers_plane->indices.size () < MINPOINTSPLANE){	// Without downsampling it is 3000
			std::cerr << "Could not estimate a planar model for the given dataset. Size: "<< inliers_plane->indices.size () << std::endl;
			j=8;
			break;
		}
		//plane = true;

		// Extract the planar inliers from the input cloud
		extract.setInputCloud (cloud_filtered2);	//
		extract.setIndices (inliers_plane);		// Marking the points that belong to the plane
		extract.setNegative (false);

//		// Write the planar inliers to disk
		//pcl::PointCloud<PointT>::Ptr cloud_plane (new pcl::PointCloud<PointT> ());
		extract.filter (*cloud_plane);		//Extracting the planar points that were marked earlier
		std::cerr << "Planar Component Before Statistical Filtering: " << cloud_plane->points.size () << " data points." << std::endl;
		std::cerr << "Plane coefficients: " << *coefficients_plane << std::endl;
		std::stringstream ss, sa;
//		if(j==2)
		{
			ss << "../output/plane_before_" << j << ".pcd";
			writer.write<pcl::PointXYZ> (ss.str (), *cloud_plane, false);
		}
//****************************************************Performing Statistical Outlier Removal Technique****************************************************//
		if(j!=0)
		{	
			if(cloud_plane->points.size () > 2000)
			{	
				cloud_plane.swap(cloud);
				pcl::VoxelGrid<PointT> sorr;
				sorr.setInputCloud (cloud);
				sorr.setLeafSize (0.01f, 0.01f, 0.01f);
				sorr.filter (*cloud_plane);
				std::cerr << "PointCloud after downsampling has: " << cloud_plane->points.size () << " data points." << std::endl;			
			}

			sor.setInputCloud (cloud_plane);
			sor.setMeanK (3500);		// Originally it's 50
			sor.setStddevMulThresh (0.5);
			sor.filter (*cloud_plane);
			std::cerr << "Planar Component After Statistical Filtering: " << cloud_plane->points.size () << " data points." << std::endl;
		}

//		viewer->addPolygon<pcl::PointXYZ> (cloud_plane, 1.0, 0.0, 0.0, "Polygon: " + j, 0);
		//std::cerr << "PointCloud representing the planar component: " << cloud_plane->points.size () << " data points." << std::endl;
//		writer.write ("../output/plane_" + j + ".pcd", *cloud_plane, false);

		
//		if(j==2)
		{
			sa << "../output/plane_after_" << j << ".pcd";
			writer.write<pcl::PointXYZ> (sa.str (), *cloud_plane, false);
		}


		// Remove the planar inliers, extract the rest
		extract.setNegative (true);
		extract.filter (*cloud_filtered2);			//Extracting the rest of the point cloud (point cloud minus the plane)
		extract_normals.setNegative (true);
		extract_normals.setInputCloud (cloud_normals);
		extract_normals.setIndices (inliers_plane);
		extract_normals.filter (*cloud_normals);		//Extracting point normals of the rest of the point clouds after filtering out NORMALS belonging to the plane
		

		pcl::PointXYZRGB minPR, maxPR,originR;
		pcl::PointXYZ minPt, maxPt,origin,oPt,nPt,dir,planeO;
		origin.x = 0;
		origin.y = 0;
		origin.z = 0;

		pcl::getMinMax3D (*cloud_plane, minPt, maxPt);
//		minPt.z = maxPt.z
		oPt.x = (minPt.x + maxPt.x)/2;
		oPt.y = (minPt.y + maxPt.y)/2;
		oPt.z = (minPt.z + maxPt.z)/2;
		
		planeO.x = maxPt.x;
		planeO.y = minPt.y;
		planeO.z = maxPt.z;

		double planePoints[3][3];		// Default: Plane approximately on the x-y plane
		planePoints[0][0] = maxPt.x;
		planePoints[0][1] = minPt.y;
		planePoints[0][2] = maxPt.z;
		planePoints[1][0] = maxPt.x;
		planePoints[1][1] = maxPt.y;
		planePoints[1][2] = maxPt.z;
		planePoints[2][0] = minPt.x;
		planePoints[2][1] = minPt.y;
		planePoints[2][2] = minPt.z;

		if(abs(coefficients_plane->values[1]) > 0.9){	// Plane approximately on the z-x plane
			planePoints[0][2] = minPt.z;
		}

		if(abs(coefficients_plane->values[0]) > 0.8){	// Plane approximately on the y-z plane
			planePoints[0][0] = maxPt.x;
			planePoints[0][1] = minPt.y;
			planePoints[0][2] = minPt.z;
			planePoints[1][0] = maxPt.x;
			planePoints[1][1] = maxPt.y;
			planePoints[1][2] = minPt.z;
			planePoints[2][0] = minPt.x;
			planePoints[2][1] = minPt.y;
			planePoints[2][2] = maxPt.z;
		}

		if(j==1){
//			planePoints[0][2] = minPt.z;
//			planeO.x = maxPt.x;
//			planeO.y = minPt.y;
//			planeO.z = minPt.z;
//			viewer->addLine<pcl::PointXYZ> (origin,minPt, "line"+j+100);
//			viewer->addLine<pcl::PointXYZ> (origin,maxPt, "line"+j+200);
//			viewer->addLine<pcl::PointXYZ> (origin,planeO, "line"+j+300);

		}

		

		pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_planePoints (new pcl::PointCloud<pcl::PointXYZ>);
		pcl::PointCloud<pcl::PointXYZ>::Ptr planePoints_projected (new pcl::PointCloud<pcl::PointXYZ>);

		// Fill in the cloud data
		cloud_planePoints->width  = 3;
		cloud_planePoints->height = 1;
		cloud_planePoints->points.resize (cloud_planePoints->width * cloud_planePoints->height);

		for (size_t ii = 0; ii < cloud_planePoints->points.size (); ++ii){
			cloud_planePoints->points[ii].x = planePoints[ii][0];
			cloud_planePoints->points[ii].y = planePoints[ii][1];
			cloud_planePoints->points[ii].z = planePoints[ii][2];
		}

		// Create the filtering object
		pcl::ProjectInliers<pcl::PointXYZ> proj;
		proj.setModelType (pcl::SACMODEL_PLANE);
		proj.setInputCloud (cloud_planePoints);
		proj.setModelCoefficients (coefficients_plane);
		proj.filter (*planePoints_projected);

		for (size_t ii = 0; ii < cloud_planePoints->points.size (); ++ii){
			planePoints[ii][0] = planePoints_projected->points[ii].x ;
			planePoints[ii][1] = planePoints_projected->points[ii].y ;
			planePoints[ii][2] = planePoints_projected->points[ii].z ;
		}

		
		if(plane){
			plane_coeff.values.resize (4);		// We need 4 values
			plane_coeff.values[0] = coefficients_plane->values[0];
			plane_coeff.values[1] = coefficients_plane->values[1];
			plane_coeff.values[2] = coefficients_plane->values[2];
			plane_coeff.values[3] = coefficients_plane->values[3]; 
			//if(j!=2)
			{
				viewer->addPlane (plane_coeff, oPt.x, oPt.y, oPt.z, "Plane: " + j, 0, planePoints);
				viewer->setShapeRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, .2 + .2 * (j), 0, 0, "Plane: " + j, 0); 
			}
			//plane = false;
		}

		//Extracting Planes from the point cloud
		// While 20% of the original cloud is still there so that small outliers can be filtered out
		if(j==0){
			seg_plane.setDistanceThreshold (0.04);
		}
		j++;
	}
	// Writing the point cloud containing all the cylinders
	//std::cerr << "PointCloud that remains after Plane extraction: " << cloud_filtered2->points.size () << " data points." << std::endl<< std::endl<< std::endl;
	//writer.write ("../output/cylinders_all.pcd", *cloud_filtered2, false);


	//******************************************for SPHERE segmentation/extraction*********************************************************************************************************
//	while (cloud_filtered2->points.size () > 0.1 * nr_points && go_sphere){			// Without downsampling
	while (go_sphere ){
		// Segment the largest spherical component from the remaining cloud
		std::cout<<"\n\n\n************************************Commencing Sphere segmentation:**********************************\n"<<std::endl;
		seg_sphere.setInputCloud (cloud_filtered2);
		seg_sphere.setInputNormals (cloud_normals);
		seg_sphere.segment (*inliers_sphere, *coefficients_sphere);
		if (inliers_sphere->indices.size () < 1100){
			std::cerr << "Could not estimate a spherical model for the given dataset." << std::endl;
			go_sphere=false;
			break;
		}
		std::cerr << "Sphere coefficients: " << *coefficients_sphere << std::endl;
    
		// Extract the inliers
		extract.setInputCloud (cloud_filtered2);
		extract.setIndices (inliers_sphere);
		extract.setNegative (false);
		extract.filter (*cloud_sphere);	
		extract_normals.setNegative (true);
		extract_normals.setInputCloud (cloud_normals);
		extract_normals.setIndices (inliers_sphere);
		extract_normals.filter (*cloud_normals);		//Extracting point normals of the rest of the point clouds after filtering out NORMALS belonging to the plane
	
		if (cloud_sphere->points.size () < 300){
			extract.setNegative (true);
			extract.filter (*cloud_filtered2);
			continue;
		}

		sphere_coeff.values.resize (4);
		sphere_coeff.values[0] = coefficients_sphere->values[0];
		sphere_coeff.values[1] = coefficients_sphere->values[1];
		sphere_coeff.values[2] = coefficients_sphere->values[2];
		sphere_coeff.values[3] = coefficients_sphere->values[3];
		//if(k==0)
		viewer->addSphere (sphere_coeff,"Sphere: " + k, 0);
		viewer->setShapeRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 0, 1, 0, "Sphere: " + k, 0); 
      	
		std::cerr << "PointCloud representing the spherical component: " << cloud_sphere->width * cloud_sphere->height << " data points." << std::endl;
	
		//std::stringstream ss;
		//ss << "../output/sphere_" << k << ".pcd";
		//writer.write<PointTC> (ss.str (), *cloud_sphere, false);
	
		// Create the new filtering object
		extract.setNegative (true);
		extract.filter (*cloud_filtered2);		
		k++;
	}
	

	//******************************************for CYLINDER segmentation/extraction*********************************************************************************************************
	cloud_normals.swap (cloud_normals2); 
//	while ((cloud_filtered2->points.size () > 0.1 * nr_points) && go_cylinder){	// Without downsampling
	    while (i < 4)    
    {
	std::cout<<"\n\n\n*************************************Commencing Cylinder segmentation:***********************************\n"<< i <<std::endl;
      // Segment the largest cylindrical component from the remaining cloud
	std::cout<<"cloud_filtered2: "<< cloud_filtered2->points.size () <<"\tcloud_normals: "<< cloud_normals->points.size () <<std::endl;
	seg_cylinder.setInputCloud (cloud_filtered2);
	seg_cylinder.setInputNormals (cloud_normals);
    
        // Obtain the cylinder inliers and coefficients
	seg_cylinder.segment (*inliers_cylinder, *coefficients_cylinder);
	//if (inliers_cylinder->indices.size () == 0)
	if (inliers_cylinder->indices.size () < MINPOINTSCYLINDER)
        {
		std::cerr << "PointCloud representing the cylindrical component: " << inliers_cylinder->indices.size () << " data points. Minimum # of points requried: "<< MINPOINTSCYLINDER << std::endl;
		go_cylinder=false;
		i++;
		break;
        }

	// Extract the cylindrical inliers from the input cloud
	extract.setInputCloud (cloud_filtered2);	//setting input cloud to the extractor
	extract.setIndices (inliers_cylinder);	//setting the inliers (plane) point list
	extract.setNegative (false);			//the inliers points are to be extracted
	pcl::PointCloud<PointT>::Ptr cloud_cylinder (new pcl::PointCloud<PointT> ());
	extract.filter (*cloud_cylinder);
  
	// Write the cylindrical inliers to disk
	if (cloud_cylinder->points.empty ()) {
		std::cerr << "Can't find the cylindrical component." << std::endl;
		go_cylinder=false;
		break;
	} 
   /*  else if(cloud_cylinder->points.size () <= 0.05 * nr_points)
    {	
	// Remove the cylindrical inliers, extract the rest
        extract.setNegative (true);		//rest of the points are to be extracted
        extract.filter (*cloud_filtered);	//extracting the rest of the points (minus inlier points)
        cloud_filtered.swap (cloud_filtered2);

	//Remove the Normals that correspond to the inlier points
	extract_normals.setInputCloud (cloud_normals2);
	extract_normals.setIndices (inliers_cylinder);
	extract_normals.setNegative (true);		//rest of the points are to be extracted
	extract_normals.filter (*cloud_normals);
	cloud_normals.swap (cloud_normals2);    

	std::cerr << "PointCloud representing the cylindrical component has too few data points: " << cloud_cylinder->points.size () << std::endl;
	go_cylinder=false;
	break;	
    } 
*/
    std::cout<<"Cylinder: "<<i<<std::endl;
    std::cerr << "PointCloud representing the cylindrical component: " << cloud_cylinder->points.size () << " data points." << std::endl;
    cylinder_coeff.values.resize (7);		// We need 7 values
/*    cylinder_coeff.values[0] = coefficients_cylinder->values[0];
    cylinder_coeff.values[1] = coefficients_cylinder->values[1];
    cylinder_coeff.values[2] = coefficients_cylinder->values[2];
    cylinder_coeff.values[3] = coefficients_cylinder->values[3];//a
    cylinder_coeff.values[4] = coefficients_cylinder->values[4];//b
    cylinder_coeff.values[5] = coefficients_cylinder->values[5];//c*/


    float a = coefficients_cylinder->values[0];//a
    float b = coefficients_cylinder->values[1];//b
    float c = coefficients_cylinder->values[2];//c
    float d = coefficients_cylinder->values[3];//a
    float e = coefficients_cylinder->values[4];//b
    float f = coefficients_cylinder->values[5];//c
    float g = coefficients_cylinder->values[6];//c


    pcl::PointXYZRGB minPR, maxPR,originR;
    pcl::PointXYZ minPt, maxPt,origin,oPt,nPt,dir;
    dir.x=d;
    dir.y=e;
    dir.z=f;

    oPt.x = a;
    oPt.y = b;
    oPt.z = c;
    
    pcl::getMinMax3D (*cloud_cylinder, minPt, maxPt);
 
    originR.x=0;
    originR.y=0;
    originR.z=0;

    originR.r=170;
    originR.g=0;
    originR.b=2;

    minPR.x=minPt.x;
    minPR.y=minPt.y;
    minPR.z=minPt.z;

    minPR.r=170;
    minPR.g=0;
    minPR.b=2;

    maxPR.x=maxPt.x;
    maxPR.y=maxPt.y;
    maxPR.z=maxPt.z;
    
    maxPR.r=170;
    maxPR.g=0;
    maxPR.b=2;


 /*   float hyp = sqrt(((maxPt.x-a)*(maxPt.x-a)) + ((maxPt.y-b)*(maxPt.y-b)) + ((maxPt.z-c)*(maxPt.z-c)));
    float h = sqrt(hyp*hyp - g*g);
    pcl::PointXYZ nor, pt;        
    float mod = sqrt(d*d + e*e + f*f);
    nor.x = d/mod;
    nor.y = e/mod;
    nor.z = f/mod;
    
    pt.x = nor.x * h;
    pt.y = nor.y * h;
    pt.z = nor.z * h;
*/
    pcl::PointXYZ pt, v, nor;        

    v.x =  a - maxPt.x;//-a;
    v.y =  b - maxPt.y;//-b;
    v.z =  c - maxPt.z;//-c;
    float len = (v.x*d)+(v.y*e)+(v.z*f);
//    std::cout<<"Cylinder "<<i<<": "<<len<<std::endl;
    float mod = sqrt(d*d + e*e + f*f);
    
    nor.x = d/mod;
    nor.y = e/mod;
    nor.z = f/mod;
    
    pt.x = nor.x * len;
    pt.y = nor.y * len;
    pt.z = nor.z * len;

//    if(i!=3)
    {
	a = a - pt.x ;
	b = b - pt.y ;
	c = c - pt.z ;
    }
    

    nPt.x = a;
    nPt.y = b;
    nPt.z = c;
//    viewer->addLine<pcl::PointXYZ> (origin,dir, "line"+i+200);
//    viewer->addLine<pcl::PointXYZ> (oPt,nPt, "line"+i+100);    
    float hypo = sqrt((maxPt.x-minPt.x)*(maxPt.x-minPt.x) + (maxPt.y-minPt.y)*(maxPt.y-minPt.y) + (maxPt.z-minPt.z)*(maxPt.z-minPt.z));
    float di = 2 * g;
    float height = sqrt((hypo*hypo)-(di*di));
    float div = height / sqrt(a*a+b*b+c*c);
//    std::cout<<"div: "<<div<<endl;

    int sign=1;  
    if (e>0) sign=-1;

    if(i==3)	//Setting the model coefficients for the first cylinder model
    {
/*	a = 0;
	b = 0;
	c = 0;
*/      //sign = sign*-1;
	d = d*div*sign;
	e = e*div*sign;
	f = f*div*sign;	
	cylinder_coeff1.values.resize (7);		// We need 7 values
	cylinder_coeff1.values[0] = 0.5;//-0.00910102;
	cylinder_coeff1.values[1] = 0.5;//0.145887;
	cylinder_coeff1.values[2] = 0.5;//1.07034;
	cylinder_coeff1.values[3] = 0.0113085;
	cylinder_coeff1.values[4] = -0.975598;
	cylinder_coeff1.values[5] = 0.219274;
	cylinder_coeff1.values[6] = 0.0350342;
//	viewer->addCylinder (cylinder_coeff1,"Cylinders: " + i);
    }
    else //if (i!=3)
    {
/*	a = a/2;
	b = b/2;
	c = c/2;
*/	d = d*div*sign;
	e = e*div*sign;
	f = f*div*sign;	
    }
//    div =0.15;

    cylinder_coeff.values[0] = a;
    cylinder_coeff.values[1] = b;
    cylinder_coeff.values[2] = c;
    cylinder_coeff.values[3] = d;
    cylinder_coeff.values[4] = e;
    cylinder_coeff.values[5] = f;

	//a=a/(sqrt(a²+b²+c²)*size
	//b=b/(sqrt(a²+b²+c²)*size
	//c=c/(sqrt(a²+b²+c²)*size
    cylinder_coeff.values[6] = g;


    if(i==2)	//Setting the model coefficients for the first cylinder model
    {
//	std::cerr << "Cylinder coefficients changed: "<< i <<" :" << cylinder_coeff << std::endl;
//	std::cerr << "maxPt x: "<< i <<" : "<< maxPt.x << std::endl;
//	std::cerr << "maxPt y: "<< i <<" : "<< maxPt.y << std::endl;
//	std::cerr << "maxPt z: "<< i <<" : "<< maxPt.z << std::endl;
	
	
    }







      
    std::cerr << "Cylinder coefficients: "<< *coefficients_cylinder << std::endl;
    //std::cerr << "cylinder_coeff: " << cylinder_coeff.values[0] << std::endl;
      
    if(cloud_cylinder->points.size ()> MINPOINTSCYLINDER ){    
	std::cerr << "Model Inserted for Cylinder: "<< i << std::endl;
	viewer->addCylinder (cylinder_coeff,"Cylinder: " + i);
	viewer->setShapeRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 0, 0, 1, "Cylinder: " + i, 0); 
    }
    else{
	std::cerr << "model NOT inserted for cylinder: "<< i << std::endl;
	go_cylinder=false;
    }

  
//    pcl::PointXYZ minPt, maxPt;
    
    pcl::getMinMax3D (*cloud_cylinder, minPt, maxPt);
    
    if(i==3 || i==4)
    {
	std::stringstream ss;
	ss << "../output/cylinder_" << i << ".pcd";
	writer.write<pcl::PointXYZ> (ss.str (), *cloud_cylinder, false);
    }

   //viewer->addLine<pcl::PointXYZRGB> (maxPR,originR, "line"+i);

    //std::cerr << "Cylinder coefficients: " << *coefficients_cylinder << std::endl;
    
  
    // Remove the cylindrical inliers, extract the rest
    extract.setNegative (true);		//rest of the points are to be extracted
    extract.filter (*cloud_filtered);	//extracting the rest of the points (minus inlier points)
    cloud_filtered.swap (cloud_filtered2);

    //Remove the Normals that correspond to the inlier points
    extract_normals.setInputCloud (cloud_normals);
    extract_normals.setIndices (inliers_cylinder);
    extract_normals.setNegative (true);		//rest of the points are to be extracted
    extract_normals.filter (*cloud_normals);

    // Create the filtering object
    i++;
    }
  }

  std::cerr<<"Visual Mode Exited!"<<std::endl;
  return (0);
}









