# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/sabdullah/Documents/shape_analysis/shape_simplification/shape_simplification.cpp" "/home/sabdullah/Documents/shape_analysis/shape_simplification/build/CMakeFiles/shape_simplification.dir/shape_simplification.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "DISABLE_LIBUSB_1_0"
  "DISABLE_PCAP"
  "DISABLE_PNG"
  "qh_QHpointer"
  "vtkDomainsChemistry_AUTOINIT=1(vtkDomainsChemistryOpenGL2)"
  "vtkFiltersStatistics_AUTOINIT=1(vtkFiltersStatisticsGnuR)"
  "vtkIOExport_AUTOINIT=1(vtkIOExportOpenGL2)"
  "vtkRenderingContext2D_AUTOINIT=1(vtkRenderingContextOpenGL2)"
  "vtkRenderingCore_AUTOINIT=3(vtkInteractionStyle,vtkRenderingFreeType,vtkRenderingOpenGL2)"
  "vtkRenderingOpenGL2_AUTOINIT=1(vtkRenderingGL2PSOpenGL2)"
  "vtkRenderingVolume_AUTOINIT=1(vtkRenderingVolumeOpenGL2)"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/vtk"
  "/usr/include/freetype2"
  "/usr/include/python2.7"
  "/usr/include/libxml2"
  "/usr/local/include/pcl-1.8"
  "/usr/include/eigen3"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
