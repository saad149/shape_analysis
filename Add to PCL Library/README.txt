1. replace the following files:
a. pcl/visualization/src/common/shapes.cpp				------> shapes.cpp
b. pcl/visualization/src/pcl_visualizer.cpp				------> pcl_visualizer.cpp
c. pcl/visualization/include/pcl/visualization/pcl_visualizer.h		------> pcl_visualizer.h
d. pcl/visualization/include/pcl/visualization/common/shapes.h		------> shapes.h

Download PCL:
https://github.com/PointCloudLibrary/pcl

Follow Steps:
http://pointclouds.org/documentation/tutorials/compiling_pcl_posix.php

1. cd pcl-pcl-1.7.2 && mkdir build && cd build

2. cmake ..

3. make -j2

4. Replace the 4 files in their appropriate directories

5. make -j2 install


