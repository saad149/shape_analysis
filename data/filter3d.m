figure;
xlabel('X(m)');
ylabel('Y(m)');
zlabel('Z(m)');
title('Original');
pc = pcread('stackcylinder.ply');
pcshow(pc);
% zmin = 0;
% zmax = 1.1;
% Z=pc.Location(:,:,3);
% s=size(Z);
% ss=s(1,1)*s(1,2);
% Z=reshape(Z,[ss,1]);
% xyz = Z(Z(:)<=zmax);
% xyz = xyz(xyz(:)<=zmax);
% indices = zeros(size(xyz));
% j=0;
%  for i=1:ss
%     if Z(i)<=zmax && Z(i) >= zmin
%         j=j+1;
%         indices(j) = i;    
%     end        
%  end 
% newpc = select(pc,indices);
% 
% figure;
% pcshow(newpc);
% xlabel('X(m)');
% ylabel('Y(m)');
% zlabel('Z(m)');
% title('After Filtering');
pcwrite(pc,'stackcylinder_b','PLYFormat','binary');
% maxDistance = 0.005;
  %roi = [0.4,0.6;-inf,0.2;0.1,inf];
%  roi = [-0.4,0.6;-0.5,0.2;1.0,1.25];
%  sampleIndices = findPointsInROI(ptCloud,roi);
%  
%  referenceVector = [0,0,1];
%  
%  model = pcfitcylinder(pc,maxDistance,referenceVector,...
%          'SampleIndices',sampleIndices);

%   model = pcfitcylinder(newpc,maxDistance);
%   hold on
%   plot(model)